import noop from 'helpers/noop';
import { createNodes } from 'helpers/dom';
import './style.css';

/**
 * @class
 */
export default class Page {
	/**
	 *
	 * @param {object} attributes -
	 */
	constructor(attributes) {
		const { route, content, parent, onClose } = attributes || {};

		this.route = route;
		this.parent = parent;
		this.content = content;
		this.onClose = onClose || noop;
	}

	/**
	 *
	 */
	render() {
		const nodes = createNodes({ template: this.content });

		nodes.forEach((node) => {
			this.parent.append(node);
		});
	}
}
