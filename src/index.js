import Application from './application';
import '../theme/index.css';

document.addEventListener('DOMContentLoaded', () => {
	console.log('DOM fully loaded and parsed');

	const application = new Application({ selector: 'application' });

	application.load();
	application.render();
});
