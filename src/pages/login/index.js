import Page from 'components/page';
import { formSerializer, createNodes } from 'helpers/dom';
import Template from './template.html';
import { validateCredential } from './validate';
import './style.css';

/**
 * @class Login
 */
export default class Login extends Page {
	constructor(attributes) {
		super({ ...attributes, content: Template });

		this.error = null;
	}

	renderForm() {
		const form = this.parent.querySelector('.form');

		form.addEventListener('submit', (event) => {
			event.preventDefault();

			return false;
		});

		form.addEventListener('change', () => {
			this.error = null;
			this.renderError();
		});
	}

	renderSubmitButton() {
		const button = this.parent.querySelector('.button[type="submit"]');

		button.addEventListener('click', (event) => {
			const form = this.parent.querySelector('.form');

			if (!form.checkValidity()) {
				event.preventDefault();

				return false;
			}

			const credentials = formSerializer(this.parent.querySelector('.form'));

			try {
				const profile = validateCredential(credentials);

				this.onClose({ profile });
				this.close();
			} catch (error) {
				this.error = error;
				this.renderError();
			}
		});
	}

	renderError() {
		if (!this.error) {
			const label = this.parent.querySelector('.text.error');

			label && label.remove();
		} else {
			const { message } = this.error;
			const template = `<label class="text error">${message}</label>`;
			const elements = createNodes({ template });
			const label = elements[0];
			const form = this.parent.querySelector('.form');

			form.insertAdjacentElement('afterend', label);
		}
	}

	render() {
		super.render();

		this.renderForm();
		this.renderSubmitButton();
	}

	close() {
		const page = this.parent.querySelector('.login.page');

		page && page.remove();
	}
}
