import usersData from 'data/users.json';

/**
 * @param {object} credential -
 * @returns {object} user
 */
export function validateCredential(credential) {
	const user = usersData.find((user) => user.username === credential.username);
	const match = user ? user.password === credential.password : false;

	if (!user || !match) {
		throw new Error('Invalid credentials');
	}

	return user.profile;
}
