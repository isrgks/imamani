import { createNodes } from 'helpers/dom';
import SidebarTemplate from './sidebar.html';

/**
 * @param {object} attributes -
 * @param {string} attributes.selector -
 */
export function renderTemplate({ selector }) {
	const nodes = createNodes({ template: SidebarTemplate, selector: '.sidebar' });
	let root = document.querySelector(selector);

	nodes.forEach((node) => {
		root.append(node);
	});
}
