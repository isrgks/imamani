import Page from 'components/page';
import Template from './template.html';

export default class Home extends Page {
	constructor(attributes) {
		super({ ...attributes, content: Template });

		this.error = null;
	}
}
