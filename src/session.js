/**
 * Read the profile data from session storage
 *
 * @returns {object} profile
 */
function readProfile() {
	return {};
}

/**
 * Write the profile data to session storage
 *
 * @param {object} profile - user's profile information
 */
function writeProfile(profile) {
	const value = JSON.stringify(profile);

	sessionStorage.setItem('profile', value);
}

/**
 * @class Session
 */
export default class Session {
	/**
	 *
	 * @param {object} attributes -
	 */
	constructor(attributes) {
		const { profile = {} } = attributes || {};

		if (Object.keys(profile).length) {
			writeProfile(profile);
		}

		this.storage = readProfile();
	}

	/**
	 * @returns {boolean} true if there is a session. Otherwise false
	 */
	hasSession() {
		return !!Object.keys(this.storage).length;
	}

	/**
	 *
	 */
	end() {
		sessionStorage.clear();
	}
}
