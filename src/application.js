import Session from './session';
import Login from './pages/login';
import Home from './pages/home';
import { renderTemplate } from './pages';

/**
 * @class
 */
export default class Application {
	/**
	 *
	 * @param {object} attributes -
	 */
	constructor(attributes) {
		const { selector } = attributes;

		this.selector = selector || 'root';
		this.page = '';
		this.session = new Session();
	}

	/**
	 *
	 */
	load() {
		let root = document.querySelector(`.${this.selector}`);

		if (!root) {
			root = document.createElement('div');
			root.classList.add(this.selector);

			document.body.prepend(root);
		}
	}

	/**
	 *
	 */
	render() {
		let parent = document.querySelector(`.${this.selector}`);

		if (this.session.hasSession()) {
			renderTemplate({ selector: `.${this.selector}` });

			this.page = new Home({ parent });
		} else {
			this.page = new Login({
				parent,
				onClose: (attributes) => {
					const { profile } = attributes;

					this.session = new Session({ profile });
					this.render();
				},
			});
		}

		this.page.render();
	}
}
