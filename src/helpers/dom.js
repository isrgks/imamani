/**
 * @private
 * @param {HTMLElement} input -
 * @returns {object} pair
 */
function serializeInput(input) {
	const { type } = input;
	let key;
	let value;

	switch (type) {
		case 'text':
		case 'password':
			key = input.name;
			value = input.value;
			break;
	}

	// create an key/value object.
	// where key is the name of the input, and the value is the value itself
	return { key: value };
}

/**
 * Creates an dom elements from a template
 *
 * @param {object} options -
 * @param {string} options.template -
 * @param {string} options.selector -
 * @param {string} options.mimeType -
 *
 * @returns {NodeList} element
 */
export function createNodes(options) {
	const parser = new DOMParser();
	const { template, selector, mimeType = 'text/html' } = options;
	const element = parser.parseFromString(template, mimeType);
	let nodes = element.body.childNodes;

	if (selector) {
		nodes = element.body.querySelectorAll(selector);
	}

	return nodes;
}

/**
 *
 * @param {HTMLFormElement} form -
 * @returns {object} -
 */
export function formSerializer(form) {
	if (!form || form.nodeName !== 'FORM' || !(form instanceof HTMLFormElement)) {
		throw new Error('Invalid form element');
	}

	const values = Array.from(form.elements).reduce((acc, element) => {
		const { nodeName } = element;
		let pair = {};

		switch (nodeName) {
			case 'INPUT':
				pair = serializeInput(element);
				break;
		}

		return { ...acc, ...pair };
	}, {});

	return values;
}
