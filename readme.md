# Web Development

This project contains a very basic Login Page. It is implement without using
additional packages except the one that are useful for compatibility Browser
features.

## Command Line Interface

There ara some custom npm scripts that are helpful for development process. Here
is the basic commands that useful for start working with this project

> See "scripts/" directory to check other additional scripts

```shell
npm install
npm run start
```

## IDE / Editor Plugin Integration

### VSCode

[ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

[HTMLHint](https://marketplace.visualstudio.com/items?itemName=mkaufman.HTMLHint)

[markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)

[stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)

## Instruction

The project is new and the team decided to start working on the Login Page, but a
new developer overwrite the git repository with inconsistent code. Once you have
noticed that, you have been tried to restore it, but the result were not the desired.
So that, your goal is **restore the code** one more time, and **fix the issues**.

First of all, you need to submit this project a new repository at: [GitLab Training](https://gitlab.fundacion-jala.org/dev-31/training/frontend-exam01)

The last time the Login Page already have the following features:

- Validate that a session exist to redirect to Home. Otherwise redirect to Login
- Show an error message "Invalid Credentials" when a user introduce invalid credentials

This mistake has delayed the release and you need implement the remaining features,
so that here is the list of new features:

- Show a message **Required Field** under the field when the field is invalid.
  Consider that the message should be hidden when user start updating the field
  This validation should be applied to Username and Password fields
- Show the **user's profile data** using a Card Component into the Home Page
  Consider that the Card Component is not completed

Since this is a new project, there are not unit tests that will help us to make big
improvements, so that make sure that at least the code **coverage is about 30%**

Use **Atomic Commits** every time you complete a task.
For example, `git commit -m "Fix building code error"` then commits by each issue

Last but not least, make sure that there are not **lint error**, you could use
`npm run lint` to check all errors
