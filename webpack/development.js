import { build } from './paths';

export default {
	mode: 'development',
	devtool: 'eval-source-map',
	devServer: {
		contentBase: build,
		clientLogLevel: 'debug',
		compress: true,
		https: true,
		port: 8443,
		hot: true,
		open: {
			app: ['Google Chrome', '--incognito'],
		},
	},
};
