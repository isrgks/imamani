import rules from './rules';
import plugins from './plugins';
import alias from './alias';
import { source, build } from './paths';

export default {
	context: source,
	entry: {
		polyfills: `${source}/helpers/polyfills.js`,
		application: `${source}/index.js`,
	},
	output: {
		filename: '[name].bundle.js',
		path: build,
	},
	resolve: { alias },
	module: { rules },
	plugins,
};
