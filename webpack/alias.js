import { root } from './paths';

export default {
	theme: `${root}/src/theme`,
	helpers: `${root}/src/helpers`,
	components: `${root}/src/components`,
	data: `${root}/src/data`,
};
