export default [
	{
		test: /\.m?js$/,
		exclude: /node_modules/,
		use: {
			loader: 'babel-loader',
			options: {
				presets: [['@babel/preset-env', { targets: 'defaults' }]],
			},
		},
	},
	{
		test: /\.css$/i,
		use: ['style-loader', 'css-loader'],
	},
	{
		test: /\.(png|jpe?g|gif|svg|woff(2)?|ttf|eot)$/i,
		use: [
			{
				loader: 'file-loader',
			},
		],
	},
	{
		test: /\.html$/i,
		loader: 'html-loader',
		options: {
			esModule: true,
		},
	},
];
